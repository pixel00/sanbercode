<?php
require ("index2.php");

$sheep = new Animal("shaun");

echo 'Name : '.$sheep->name =  "shaun<br>";
echo 'Legs : '.$sheep->legs = "4<br>";
echo 'Cold blooaded : '.$sheep->cold_blooded.'<br><br>'; // "no"

$sungokong = new Ape("kera sakti");

echo 'Name : '.$sungokong->name =  "KeraSakti<br>";
echo 'Legs : '.$sungokong->legs = "2<br>";
echo 'Cold blooaded : '.$sungokong->cold_blooded.'<br>'; // "no"
echo 'Yell : '.$sungokong->yell.'<br><br>';// "Auooo"

$kodok = new Frog("buduk");
echo 'Name : '.$kodok->name =  "Buduk<br>";
echo 'Legs : '.$kodok->legs = "4<br>";
echo 'Cold blooaded : '.$kodok->cold_blooded.'<br>'; // "no"
echo 'Jump : '.$kodok->jump; ; // "hop hop"

?>
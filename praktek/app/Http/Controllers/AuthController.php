<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

   // public function welcome(){
   // return view('welcome');
   // }

    public function welcome(Request $request){
        //dd($request->all());
        $name1 = $request['namadepan'];
        $name2 = $request['namabelakang'];
        
        return view('welcome', compact('name1', 'name2')); 
    }
    
}

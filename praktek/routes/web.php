<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/adminlte', function() {
    return view('adminlte');
});
Route::get('/adminlte/index1', function() {
    return view('clone.index1');
});

Route::get('adminlte/index2', function() {
    return view('clone.index2');
});
//Route::post('/welcome', 'AuthController@welcome_post');




